const serverlessApiUrl = process.env.REACT_APP_API_URL;

const http = async (url, method, body) => {
  const res = await fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('id_token')}`,
    },
    body: JSON.stringify(body),
  });
  return res.json();
};

export const postItem = (body) => http(`${serverlessApiUrl}/items`, 'post', body);

export const getItems = () => http(`${serverlessApiUrl}/items`, 'get');

export const editItem = (id, body) => http(`${serverlessApiUrl}/items/${id}`, 'put', body);

export const deleteItems = (id) => http(`${serverlessApiUrl}/items/${id}`, 'delete');

// export const login = (email) => http(`${serverlessApiUrl}/auth/${id}`, 'delete');
