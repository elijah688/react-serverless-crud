import React, { createContext, useCallback, useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { postItem, getItems, deleteItems, editItem } from '../api';

export const editingActions = {
  BEGIN_EDITING: 'startEditing',
  ENABLE_SUBMITTING: 'enableSubmitting',
  COMPLETE_EDITING: 'completeEditing',
};

export const Context = createContext();

const ContextProvider = ({ children }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const loadData = useCallback(async () => {
    try {
      const newItems = await getItems();
      setItems(newItems.items);
      setLoading(false);
    } catch (error) {
      history.push('/error');
    }
  }, [history]);

  const postData = useCallback(
    async (item) => {
      try {
        setLoading(true);
        await postItem(item);
        loadData();
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
        history.push('/error');
      }
    },
    [loadData, history],
  );

  const editData = useCallback(
    async (id, data) => {
      try {
        setLoading(true);
        await editItem(id, data);
        loadData();
      } catch (error) {
        history.push('/error');
      }
    },
    [loadData, history],
  );

  const deleteData = useCallback(
    async (id) => {
      try {
        setLoading(true);
        await deleteItems(id);
        loadData();
      } catch (error) {
        history.push('/error');
      }
    },
    [loadData, history],
  );

  return (
    <Context.Provider value={{ items, postData, deleteData, editData, loading, loadData }}>
      {children}
    </Context.Provider>
  );
};

ContextProvider.propTypes = {
  children: PropTypes.object,
};

ContextProvider.defaultProps = {
  children: {},
};

export default ContextProvider;
