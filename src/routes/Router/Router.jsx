import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import routes from '../routes';
import ContextProvider from '../../state/Context';

const Router = () => (
  <BrowserRouter>
    <ContextProvider>
      <Switch>
        {routes().map((r, i) => (
          <Route key={i} path={r.path} render={r.render} exact={r.exact} />
        ))}
        <Redirect to="/auth" />
      </Switch>
    </ContextProvider>
  </BrowserRouter>
);

export default Router;
