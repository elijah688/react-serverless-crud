import React from 'react';
import Items from '../pages/Items/Items';
import Error from '../components/Error/Error';
import Authentication from '../components/Authentication/Authentication';

const routes = () => [
  { path: '/auth', render: () => <Authentication />, exact: true },
  { path: '/items', render: () => <Items />, exact: true },
  { path: '/error', render: () => <Error />, exact: true },
];

export default routes;
