import React, { useState } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { CognitoUserPool, AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';
import { useForm } from 'react-hook-form';
import './Authentication.scss';
import { useHistory } from 'react-router-dom';

const Authentication = () => {
  const history = useHistory();
  const { register, handleSubmit } = useForm();
  const [isLogin, setIsLogin] = useState(false);
  const userPool = new CognitoUserPool({
    UserPoolId: process.env.REACT_APP_POOL_ID,
    ClientId: process.env.REACT_APP_CLIENT_ID,
  });

  const login = (username, password) => {
    const authenticationData = {
      Username: username,
      Password: password,
    };

    const user = new CognitoUser({ Username: username, Pool: userPool });
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) =>
      user.authenticateUser(authenticationDetails, {
        onSuccess: (result) => resolve(result.getIdToken().getJwtToken()),
        onFailure: (err) => reject(err),
      }),
    );
  };

  const signUp = (username, password) => {
    userPool.signUp(username, password, [], null, (err, data) => {
      // eslint-disable-next-line no-console
      if (err) console.error(err);
      // eslint-disable-next-line no-console
      console.log(data);
    });
  };

  const signOut = () => {
    userPool.getCurrentUser().signOut();
    localStorage.removeItem('id_token');
  };

  const submit = async (values) => {
    const { username, password } = values;
    if (isLogin) {
      try {
        const userToken = await login(username, password);
        // eslint-disable-next-line no-alert
        alert(userToken);
        localStorage.setItem('id_token', userToken);
        history.push('/items');
      } catch (e) {
        // eslint-disable-next-line no-alert
        alert(e);
      }
      return;
    }
    signUp(username, password);
  };

  return !userPool.getCurrentUser() ? (
    <Card className="Auth">
      <Card.Body>
        <Card.Title>{isLogin ? 'Login' : 'Sign Up'}</Card.Title>
        <Form onSubmit={handleSubmit(submit)}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control name="username" ref={register()} type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">
              We ll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control name="password" ref={register()} type="password" placeholder="Password" />
          </Form.Group>
          <Button variant="warning" type="submit">
            {isLogin ? 'Login' : 'Sign Up'}
          </Button>
        </Form>
      </Card.Body>

      <Card.Text>
        <Button variant="outline-warning" onClick={() => setIsLogin(!isLogin)}>
          {isLogin ? 'Would you likt to Sign Up?' : 'Would you likt to Login?'}
        </Button>
      </Card.Text>
    </Card>
  ) : (
    <Card className="Logout">
      <Card.Body>
        <Card.Title>Log Out</Card.Title>
      </Card.Body>
      <Card.Text>Would you like to Log Out?</Card.Text>
      <Button
        onClick={() => {
          signOut();
          history.push('/');
        }}
      >
        Log Out
      </Button>
    </Card>
  );
};
export default Authentication;
