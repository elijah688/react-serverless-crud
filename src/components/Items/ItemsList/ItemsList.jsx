import React, { useContext, useEffect } from 'react';
import { Spinner } from 'react-bootstrap';
import SingleItem from '../SingleItem';
import './ItemsList.scss';
import { Context } from '../../../state/Context';

const ItemsList = () => {
  const { items, loading, loadData } = useContext(Context);

  useEffect(() => {
    loadData();
  }, [loadData]);

  return loading ? (
    <Spinner className="Spinner" variant="warning" animation="border" role="status">
      <span className="sr-only">Loading...</span>
    </Spinner>
  ) : (
    items.map((item) => (
      <SingleItem key={item.id} id={item.id} title={item.title} content={item.content} />
    ))
  );
};

export default ItemsList;
