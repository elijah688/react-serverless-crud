import React, { useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import classes from './ItemsForm.module.scss';
import { Context } from '../../../state/Context';

const ItemsForm = () => {
  const { postData } = useContext(Context);

  const { register, handleSubmit, errors } = useForm();
  return (
    <Form
      className={classes.Form}
      onSubmit={handleSubmit(async (data) => {
        postData(data);
      })}
    >
      <Form.Group controlId="itemTitle">
        <Form.Label>Title</Form.Label>
        <Form.Control
          name="title"
          ref={register({ required: true })}
          type="text"
          placeholder="Enter title..."
        />
        {errors.title && <span className={classes.error}>This field is required</span>}
      </Form.Group>
      <Form.Group controlId="itemContent">
        <Form.Label>Content</Form.Label>
        <Form.Control
          name="content"
          ref={register({ required: true })}
          type="text"
          as="textarea"
          ros="3"
          placeholder="Enter Content Here..."
        />
        {errors.content && <span className={classes.error}>This field is required</span>}
      </Form.Group>
      <Form.Group className={classes.Controls} controlId="itemControls">
        <Button variant="warning" type="submit">
          Submit
        </Button>
      </Form.Group>
    </Form>
  );
};

export default ItemsForm;
