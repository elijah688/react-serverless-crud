import React, { useContext, useReducer, useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Button, Form } from 'react-bootstrap';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import DeleteButton from './DeleteButton/DeleteButton';
import './SingleItem.scss';
import { Context, editingActions } from '../../../state/Context';

const initialState = {
  editing: false,
  submitting: false,
};
const editingReducer = (state, action) => {
  switch (action.type) {
    case editingActions.BEGIN_EDITING:
      return { ...state, editing: true, submitting: false };
    case editingActions.ENABLE_SUBMITTING:
      return { ...state, editing: true, submitting: true };
    case editingActions.COMPLETE_EDITING:
      return { ...state, editing: false, submitting: false };
    default:
      throw new Error('Error: editingReducer encountered bad state!');
  }
};

const schema = yup.object().shape({
  title: yup.string().required().min(3),
  content: yup.string().required().min(3),
});

const SingleItem = ({ title, content, id }) => {
  const { deleteData, editData } = useContext(Context);
  const [editingState, dispatch] = useReducer(editingReducer, initialState);
  const { editing, submitting } = editingState;
  const [errors, setErrors] = useState();
  const { register, handleSubmit } = useForm({
    defaultValues: { title, content },
  });

  const submit = async (values) => {
    dispatch({ type: editingActions.ENABLE_SUBMITTING });
    if (submitting) {
      try {
        await schema.validate(values, { abortEarly: false });
        dispatch({ type: editingActions.COMPLETE_EDITING });
        editData(id, values);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
        setErrors(errors.error);
      }
    }
  };
  return (
    <Card className="Card">
      <Card.Header>Item</Card.Header>
      <Card.Body>
        {editing ? (
          <Form id="ItemDetails" onSubmit={handleSubmit(submit)}>
            <Form.Group controlId="ItemDetails">
              <Form.Label>Title</Form.Label>
              <Form.Control
                name="title"
                ref={register()}
                type="text"
                placeholder="Enter Title..."
              />
              {errors
                && errors
                  .filter((error) => error.includes('title'))
                  .map((error) => <Form.Text className="error">{error}</Form.Text>)}
              <Form.Label>Content</Form.Label>
              <Form.Control
                name="content"
                ref={register()}
                type="text"
                placeholder="Enter Content...."
              />
              {errors
                && errors
                  .filter((error) => error.includes('content'))
                  .map((error) => <Form.Text className="error">{error}</Form.Text>)}
            </Form.Group>
          </Form>
        ) : (
          <>
            <Card.Title>{title}</Card.Title>
            <Card.Text>{content}</Card.Text>
          </>
        )}
      </Card.Body>
      <Card.Footer className="card-footer">
        <Button
          form={editing ? 'ItemDetails' : null}
          type={editing && 'submit'}
          variant="warning"
          onClick={() => !editing && dispatch({ type: editingActions.BEGIN_EDITING })}
        >
          {editing ? 'Submit' : 'Edit'}
        </Button>
        <DeleteButton onClick={() => deleteData(id)} />
      </Card.Footer>
    </Card>
  );
};

SingleItem.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default SingleItem;
