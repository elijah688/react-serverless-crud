import React from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import ItemsForm from '../../components/Items/ItemsForm';
import ItemsList from '../../components/Items/ItemsList';
import './Items.scss';

const Items = () =>
  (localStorage.getItem('id_token') ? (
    <Container>
      <Row className="justify-content-md-center mt-5">
        <Col md={{ span: 6 }}>
          <ItemsForm />
        </Col>
      </Row>
      <Row className="justify-content-md-center mt-5">
        <Col className="ItemsCol" md={{ span: 6 }}>
          <ItemsList />
        </Col>
      </Row>
    </Container>
  ) : (
    <Redirect to="/auth" />
  ));

export default Items;
