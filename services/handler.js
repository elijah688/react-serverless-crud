const { DynamoDB } = require('aws-sdk');
const { postItems } = require('./modules/postItem');
const { getById } = require('./modules/getById');
const { putItem } = require('./modules/putItem');
const { getAll } = require('./modules/scan');
const { deleteItem } = require('./modules/deleteItem');

const ddb = new DynamoDB.DocumentClient();

const tableName = process.env.DYNAMODB_TABLE;

exports.postItem = async (event) => {
  const res = await postItems(event, ddb, tableName);
  return res;
};

exports.getAll = async (event) => {
  const res = await getAll(event, ddb, tableName);
  return res;
};

exports.getById = async (event) => {
  const res = await getById(event, ddb, tableName);
  return res;
};

exports.putItem = async (event) => {
  const res = await putItem(event, ddb, tableName);
  return res;
};

exports.deleteItem = async (event) => {
  const res = await deleteItem(event, ddb, tableName);
  return res;
};
