const { setHeaders } = require('./headers');

exports.deleteItem = async (event, ddb, tableName) => {
  const { id } = event.pathParameters;
  const params = {
    TableName: tableName,
    Key: { id },
  };

  const responseBody = {};
  let statusCode = 200;

  try {
    await ddb.delete(params).promise();
    responseBody.message = 'Success!';
  } catch (error) {
    statusCode = 500;
    responseBody.error = error;
  }

  return {
    statusCode,
    headers: setHeaders(),
    body: JSON.stringify(responseBody),
  };
};
