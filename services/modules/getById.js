const { setHeaders } = require('./headers');

exports.getById = async (event, ddb, tableName) => {
  let statusCode = 200;
  const responseBody = {};
  try {
    const { id } = event.pathParameters;
    const params = {
      TableName: tableName,
      KeyConditionExpression: 'id = :id',
      ExpressionAttributeValues: {
        ':id': id,
      },
    };

    const response = await ddb.query(params).promise();
    responseBody.items = response.Items;
  } catch (error) {
    statusCode = 500;
    responseBody.error = error;
  }

  return {
    statusCode,
    headers: setHeaders(),
    body: JSON.stringify(responseBody),
  };
};
