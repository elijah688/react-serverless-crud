const { setHeaders } = require('./headers');

exports.putItem = async (event, ddb, tableName) => {
  const { id } = event.pathParameters;
  const { title, content } = JSON.parse(event.body);

  let statusCode = 200;
  const responseBody = {};

  const params = {
    TableName: tableName,
    Item: {
      id,
      title,
      content,
    },
  };

  try {
    await ddb.put(params).promise();
    responseBody.message = 'Success!';
  } catch (error) {
    statusCode = 500;
    responseBody.error = error;
  }

  return {
    statusCode,
    headers: setHeaders(),
    body: JSON.stringify(responseBody),
  };
};
