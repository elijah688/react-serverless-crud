const { setHeaders } = require('./headers');

exports.getAll = async (event, ddb, tableName) => {
  let statusCode = 200;
  const responseBody = {};
  try {
    const params = {
      TableName: tableName,
    };
    const items = await ddb.scan(params).promise();
    statusCode = 200;
    responseBody.items = items.Items;
  } catch (error) {
    statusCode = 500;
    responseBody.error = error;
  }

  return {
    statusCode,
    headers: setHeaders(),
    body: JSON.stringify(responseBody),
  };
};
