const { v4: uuidv4 } = require('uuid');
const { setHeaders } = require('./headers');

exports.postItems = async (event, ddb, tableName) => {
  const { title, content } = JSON.parse(event.body);

  let statusCode = 200;
  const responseBody = {};
  const params = {
    TableName: tableName,
    Item: {
      id: uuidv4(),
      title,
      content,
    },
  };

  try {
    await ddb.put(params).promise();
    responseBody.message = 'Success!';
  } catch (error) {
    statusCode = 500;
    responseBody.error = error;
  }

  return {
    statusCode,
    headers: setHeaders(),
    body: JSON.stringify(responseBody),
  };
};
